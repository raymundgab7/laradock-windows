#!/bin/bash

docker-compose build nginx mysql
docker-compose up -d nginx mysql
docker-compose ps

winpty docker-compose exec --user=laradock workspace bash

read -p "DONE! Press enter to close."